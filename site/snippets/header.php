<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $site->title()->esc() ?> &mdash; <?= $page->title()->esc() ?></title>

    <link rel="icon" type="image/png" href="/assets/favicons/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/svg+xml" href="/assets/favicons/favicon.svg" />
    <link rel="shortcut icon" href="/assets/favicons/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicons/apple-touch-icon.png" />
    <meta name="apple-mobile-web-app-title" content="MyWebSite" />
    <link rel="manifest" href="/assets/favicons/site.webmanifest" />

    <?= css([
      'assets/normalize.css',
      'assets/styles.css',
    ]) ?>

</head>
<body class="page-index">

    <header class="main">
        <a class="logo" href="<?= $site->url() ?>"><img src="/assets/svg/Sifon-Logo.svg" alt="Sifon Logo"></a>
        <div class="container-buttons">
            <button type="button" class="nav-button">
                <img class="nav-closed" src="/assets/svg/Burger.svg" alt="Navigation Open Icon">
                <img class="nav-open" src="/assets/svg/Burger-Close.svg" alt="Navigation Close Icon">
            </button>
            <button type="button" class="shuffle"><img src="/assets/svg/Shuffle.svg" alt="Suffle"></button>
            <button type="button" class="page-down"><img src="/assets/svg/Page-Down.svg" alt="Page Down"></button>
        </div>
    </header>
    <?php

        $items = $pages->listed()->notTemplate('project');

    ?>
    <nav>
        <div class="floating-wrapper">
            <ul>
                <?php foreach($items as $item): ?>
                <li><a<?php e($item->isOpen(), ' class="active"') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a></li>
                <?php endforeach ?>
            </ul>
        </div>
    </nav>
